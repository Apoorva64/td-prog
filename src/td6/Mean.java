package td6;

import java.io.*;
import java.util.Scanner;

public class Mean {
    public static void main(String[] args) {

        Scanner scanner;
        try {
            // open notes
            scanner = new Scanner(new FileInputStream("./data/td6_text/LesNotes.txt"));
            // create file LesMoyennes
            FileWriter fileWriter = new FileWriter("./data/td6_text/LesMoyennes.txt");
            double classSum = 0;
            int studentCount = 0;
            while (scanner.hasNextLine()) {
                // get a line in notes
                String line = scanner.nextLine();
                // process the line
                line = line.replace(",", ".");
                String[] data = line.split(" ");
                // calculate the mean
                double studentSum = 0;
                for (int i = 2; i < data.length; i++) {
                    studentSum += Double.parseDouble(data[i]);
                }
                // Checks if the studentSum is not null
                if (studentSum != 0) {
                    double studentMean = studentSum / (data.length - 2);
                    classSum += studentMean;
                    studentCount++;

                    // write to file
                    fileWriter.write(data[0] + " " + data[1] + ": " + String.format("%,.2f", studentMean) + "\n");
                } else {
                    // write to file
                    fileWriter.write(data[0] + " " + data[1] + ": " + "abs\n");
                }


            }
            fileWriter.write("\nMoyenne Générale : " + String.format("%,.3f", classSum/studentCount) + "\n");
            fileWriter.close();
        } catch (FileNotFoundException e) {
            System.err.println("couldn't find file");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

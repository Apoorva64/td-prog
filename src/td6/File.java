package td6;

import java.io.*;
import java.util.Random;

public class File {
    private final String path;
    private final Random random = new Random();

    public File(String path) {
        this.path = path;
    }

    public int min() {
        int min = Integer.MAX_VALUE;
        try {
            DataInputStream inputStream = new DataInputStream(new FileInputStream(this.path));
            while (true) {
                try {
                    int currentNumber = inputStream.readInt();
                    if (currentNumber < min) {
                        min = currentNumber;
                    }
                } catch (EOFException e) {
                    break;
                }
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            System.err.println(this.path + " not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("couldn't read "+this.path);
            e.printStackTrace();
        }
        return min;
    }

    public void random(int n) {
        try {
            DataOutputStream fileOutputStream = new DataOutputStream(new FileOutputStream(this.path));
            for (int i = 0; i < n; i++) {
                fileOutputStream.writeInt(random.nextInt(100));
            }
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            System.err.println(this.path + " not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("couldn't write to "+this.path);
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        try {
            DataInputStream inputStream = new DataInputStream(new FileInputStream(this.path));
            string = new StringBuilder();
            while (true) {
                try {
                    string.append(inputStream.readInt()).append("\n");
                } catch (IOException e) {
                    inputStream.close();
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(this.path + " not found");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("couldn't read "+this.path);
            e.printStackTrace();
        }
        return "File{" +
                "path='" + path + '\'' +
                ", content: \n" + string +
                '}';
    }
}

package td1;

import java.awt.*;

/**
 * Classe de Test pour tester les différentes classes du td1 et 2
 *
 * @author V. Granet vg@unice.fr
 */

public class Test {
    public static void main(String[] args) {
        point_test();
        vector_test();
        segment_test();


    }

    public static void point_test() {
        // instantiating points
        Point O = new Point();
        Point A = new Point(14, -5);
        Point B = new Point(2, 7);
        // putting in array
        Point[] array = new Point[]{O, A, B};
        // print points
        for (Point point : array) {
            System.out.println(point);
        }
        // print distance
        System.out.println("distance between O and A " + O.distance(A));
        System.out.println("distance between O and B " + O.distance(B));
        System.out.println("distance between A and B " + A.distance(B));

        // equality test
        System.out.println("A==B: " + A.equals(B));
    }

    public static void vector_test() {
        // intanciating points
        Point O = new Point();
        Point A = new Point(14, -5);
        Point B = new Point(2, 7);
        Point C = new Point(5, 4);
        Vector2 AO = new Vector2(A, O);
        Vector2 OB = new Vector2(B);
        Vector2 AB = new Vector2(A, B);
        Vector2 AC = new Vector2(A, C);


        //Q8
        System.out.printf("AO.sum(OB).equals(AB): %b%n", AO.sum(OB).equals(AB));

        //Q9
        System.out.println("AC and AB collinear: " + AC.is_collinear(AB));

        //Q10

        Point D = new Point(1, 2);
        Point E = new Point(2, 0);
        Point F = new Point(3, 2);

        // check if is a parallelogram
        System.out.printf("check parallelogram, Vector OD equals Vector EF: %b %n", new Vector2(O, D).equals(new Vector2(E, F)));

    }

    public static void segment_test() {
        Plan2D plan = new Plan2D();
        // instantiating points
        Point O = new Point(0, 0, "O");
        Point A = new Point(14, -5, "A");
        Point B = new Point(2, 7, "B");
        Point C = new Point(5, 4, "C");
        Point D = new Point(1, 2, "D");
        Point E = new Point(2, 0, "E");
        Point F = new Point(3, 2, "F");
        Point G = new Point(-2, -3, "G");
        Point H = new Point(9, 5, "H");
        Point[] points_array = new Point[]{O, A, B, C, D, E, F, G, H};
        for (Point point : points_array) {
            plan.drawPoint(point, new Color(88, 90, 255), 5);
            plan.drawPointName(point, Color.BLACK, point.getName());
        }

        //Q12
        Segment AB = new Segment(A, B);

        //Q13
        System.out.printf("AB.contains(C): %b %n", AB.contains(C));
        System.out.printf("AB.contains(D): %b %n", AB.contains(D));


        //Q14
        Segment GH = new Segment(G, H);
        plan.drawSegment(AB);
        plan.drawSegment(GH);
        int point_size = 10;
        Color point_color = new Color(255, 14, 14);
        Point intersection = AB.intersection(GH);
        plan.drawPoint(intersection, point_color, point_size);
        System.out.println(intersection);

        //Q15

        System.out.printf("AB and GH contain intersection : %b %n", AB.contains(intersection) && GH.contains(intersection));


        //Q16
        Segment EF = new Segment(E, F);
        plan.drawSegment(EF);
        Point ABEF_intersection = AB.intersection(EF);
        System.out.printf("ABEF intersection exists: %b %n", ABEF_intersection != null);

        if (ABEF_intersection != null) {
            plan.drawPoint(ABEF_intersection, point_color, point_size);
        }


    }
}

package td1;

/**
 * Cette classe représente les vecteurs dans un espace à 2 dimensions
 *
 * @author V. Granet vg@unice.fr
 */
public class Vector2 {
    private final double x;
    private final double y;

    /**
     * Rôle : initialise un Vecteur2 à partir des coordonnées a et b
     *
     * @param a : x
     * @param b : y
     */
    private Vector2(double a, double b) {
        this.x = a;
        this.y = b;
    }

    /**
     * Rôle : initialise le vecteur OA
     */
    public Vector2(Point A) {
        this.x = A.getX();
        this.y = A.getY();
    }

    /**
     * Rôle : initialise le vecteur AB
     */
    public Vector2(Point A, Point B) {
        this.x = B.getX() - A.getX();
        this.y = B.getY() - A.getY();
    }

    /**
     * Rôle : renvoie le Point P(this.x, this.y) correspondant au Vecteur OP
     */
    public Point getPoint() {
        return new Point(this.x, this.y);
    }

    /**
     * Rôle : renvoie la coordonnée x du Vecteur2 courant
     */
    public double getX() {
        return this.x;
    }

    /**
     * Rôle : renvoie la coordonnée y du Vecteur2 courant
     */
    public double getY() {
        return this.y;
    }

    /**
     * Rôle : renvoie le Vecteur2 somme du Vecteur2 courant et
     * du Vecteur2 v.
     */
    public Vector2 sum(Vector2 v) {
        return new Vector2(this.getX() + v.getX(), this.getY() + v.getY());
    }

    /**
     * Rôle : renvoie le Vecteur2 produit du Vecteur2 courant et k
     */
    public Vector2 product(double k) {
        return new Vector2(this.getX() * k, this.getY() * k);
    }


    /**
     * Rôle : teste si le Vecteur2 courant et le Vecteur2 v sont égaux
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector2)) return false;
        Vector2 vector2 = (Vector2) o;
        return Double.compare(vector2.getX(), getX()) == 0 && Double.compare(vector2.getY(), getY()) == 0;
    }


    /**
     * Rôle : teste si le Vecteur2 courant et le Vecteur2 v sont colinéaires
     */
    public boolean is_collinear(Vector2 v) {
        return Segment.almostEqual(this.getX() * v.getY() - v.getX() * this.getY(), 0, Segment.eps);
    }

    /**
     * Rôle : renvoie la représentation du Vecteur2 courant sous
     * forme d'une chaîne de caratères
     */
    @Override
    public String toString() {
        return "Vector2{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

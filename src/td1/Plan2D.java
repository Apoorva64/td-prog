package td1;

import PaD.*;

import java.awt.*;

/**
 * Cette classe représente le plan cartésien et permet de visualiser à
 * l'aide de la planche à dessin (PaD) des segments de droite et des
 * points du plan
 *
 * @author V. Granet vg@unice.fr
 */

public class Plan2D {
    private final double unit = 20;
    private final PlancheADessin pad;
    private final double milieuL;
    private final double milieuH;

    /**
     * Rôle crée un Plan2D avec les axes gradués des abscisses et des ordonnées
     */
    public Plan2D() {
        this.pad = new PlancheADessin(true);
        this.milieuL = pad.getLargeur() / 2;
        this.milieuH = pad.getHauteur() / 2;
        this.drawAxes();
        this.drawGraduations();
    }

    public Plan2D(int x, int y) {
        this.pad = new PlancheADessin(x, y, false);
        this.milieuL = pad.getLargeur() / 2;
        this.milieuH = pad.getHauteur() / 2;
        this.drawAxes();
        this.drawGraduations();
    }

    public PlancheADessin getPad() {
        return pad;
    }

    /*
     * Rôle : convertit une coordonnée x du PaD en abscisse du Plan2D courant
     */
    private double coordX(double x) {
        return (unit * x) + milieuL;
    }

    /*
     * Rôle : convertit une coordonnée y du PaD en ordonnée du Plan2D courant
     */
    private double coordY(double y) {
        return -(unit * y) + milieuH;
    }

    /**
     * converts coordiantes from cartesian space to draw space
     *
     * @param point: point object in cartesian space
     * @return: point object in draw space
     */
    public Point convertCoordinates(Point point) {
        return new Point(coordX(point.getX()), coordY(point.getY()));
    }

    /*
     * Rôle : trace les axes des abscisses et des ordonnées du Plan2D courant
     */
    private void drawAxes() {
        Dessinable x_axis = new Ligne(0.0, milieuH, milieuL + milieuL, milieuH);
        Dessinable y_axis = new Ligne(milieuL, 0.0, milieuL, milieuH + milieuH);
        this.pad.ajouter(y_axis);
        this.pad.ajouter(x_axis);
    }

    /*
     * Rôle : gradue les axes des abscisses et des ordonnées du Plan2D courant
     */

    private void drawGraduations() {
        double grad_size = 5;

        for (double i = milieuL; i < pad.getLargeur(); i += 20) {
            Dessinable graduation = new Ligne(i, milieuH + grad_size, i, milieuH - grad_size);
            this.pad.ajouter(graduation);
        }
        for (double i = milieuL; i > 0; i -= 20) {
            Dessinable graduation = new Ligne(i, milieuH + grad_size, i, milieuH - grad_size);
            this.pad.ajouter(graduation);
        }


        for (double i = milieuH; i < pad.getHauteur(); i += 20) {
            Dessinable graduation = new Ligne(milieuL + grad_size, i, milieuL - grad_size, i);
            this.pad.ajouter(graduation);
        }
        for (double i = milieuH; i > 0; i -= 20) {
            Dessinable graduation = new Ligne(milieuL + grad_size, i, milieuL - grad_size, i);
            this.pad.ajouter(graduation);

        }
    }

    /**
     * Rôle : trace le Point p dans la couleur c sur le Plan2D courant
     */
    public void drawPoint(Point p, Color c, double size) {
        Dessinable point = new CerclePlein(coordX(p.getX()), coordY(p.getY()), size, c);
        this.pad.ajouter(point);
    }

    public void drawPointName(Point p, Color c, String name) {
        Dessinable text = new Texte(coordX(p.getX()), coordY(p.getY()), name, c);
        this.pad.ajouter(text);
    }

    /**
     * Rôle : trace le Point s sur le Plan2D courant
     */
    public void drawSegment(Segment s) {
        Dessinable line = new Ligne(coordX(s.getStart().getX()), coordY(s.getStart().getY()), coordX(s.getEnd().getX()), coordY(s.getEnd().getY()));
        this.pad.ajouter(line);

    }
}

package td1;

/**
 * Cette classe représente les segments de droite entre 2 points du plan.
 * Ces deux points doivent être différents.
 *
 * @author V. Granet vg@unice.fr
 */

public class Segment {
    private Point start, end;
    public static final double eps = 1E-10;

    /**
     * Rôle : initialise le Segment de droite d'orgine O et de fin a
     * Antécédent : a != 0
     */
    public Segment(Point a) {
        this.start = new Point();
        this.end = a;
    }

    /**
     * Rôle : initialise le Segment de droite d'orgine a et de fin b
     * Antécédent : a != b
     */
    public Segment(Point a, Point b) {
        this.start = a;
        this.end = b;
    }

    /**
     * Rôle : renvoie le point d'origne du Segment courant
     */
    public Point getStart() {
        return this.start;
    }

    /**
     * Rôle : renvoie le point de fin du Segment courant
     */
    public Point getEnd() {
        return this.end;
    }

    /**
     * Rôle : modifie le point d'orgine du Semgent courant
     */
    public void setStart(Point start) {
        this.start = start;
    }

    /**
     * Rôle : modifie le point de fin du Semgent courant
     */
    public void setEnd(Point end) {
        this.end = end;
    }

    /**
     * Rôle : renvoie la représentation du Segment courant sous
     * forme d'une chaîne de caratères
     */
    public String toString() {
        return "[" + this.start + "-" + this.end + "]";
    }

    // returns segment's length
    public double length() {
        return this.start.distance(this.end);

    }

    /**
     * rôle : teste si le Point p appartient au Segment courant
     */
    public boolean contains(Point p) {
        // check if collinear
        Vector2 segment_vector = new Vector2(this.start, this.end);
        Vector2 segment_point_vector = new Vector2(this.start, p);
        if (segment_vector.is_collinear(segment_point_vector)) {
            double k = segment_point_vector.getX() / segment_vector.getX();
            return 0 <= k && k <= 1;

        }
        return false;
    }

    /**
     * @param a:   first number
     * @param b:   second number
     * @param eps: tolerance
     * @return a boolean if the first number is equal to the second number with a certain tolerance
     */
    public static boolean almostEqual(double a, double b, double eps) {
        return Math.abs(a - b) < eps;
    }

    /**
     * rôle : renvoie le Point d'intersection entre le Segment courant
     * et le Segment s. Si pas de point d'intersaction, la
     * fonction renvoie null
     */

    public Point intersection(Segment s) {
        // generating vectors for the segments
        Vector2 segment_vector = new Vector2(this.getStart(), this.getEnd());
        Vector2 s_segment_vector = new Vector2(s.getStart(), s.getEnd());

        // verifying parallelism
        if (segment_vector.is_collinear(s_segment_vector)) {
            return null;
        }

        // extracting coordinates
        double x_a = this.getStart().getX();
        double y_a = this.getStart().getY();
        double x_b = this.getEnd().getX();
        double y_b = this.getEnd().getY();

        double x_c = s.getStart().getX();
        double y_c = s.getStart().getY();
        double x_d = s.getEnd().getX();
        double y_d = s.getEnd().getY();

        // applying formula
        double k_ab_numerator = (y_a - y_c) * (x_d - x_c) - (x_a - x_c) * (y_d - y_c);
        double k_ab_denominator = (x_b - x_a) * (y_d - y_c) - (y_b - y_a) * (x_d - x_c);
        double k_ab = k_ab_numerator / k_ab_denominator;

        double k_cd_numerator = (x_c - x_a) * (y_b - y_a) + (y_a - y_c) * (x_b - x_a);
        double k_cd_denominator = (y_d - y_c) * (x_b - x_a) - (x_d - x_c) * (y_b - y_a);
        double k_cd = k_cd_numerator / k_cd_denominator;


        if (!(0 <= k_ab) || !(k_ab <= 1) || !(0 <= k_cd) || !(k_cd <= 1)) {
            return null;
        }

        double x_i_ab = k_ab * (x_b - x_a) + x_a;
        double y_i_ab = k_ab * (y_b - y_a) + y_a;

        double x_i_cd = k_cd * (x_d - x_c) + x_c;
        double y_i_cd = k_cd * (y_d - y_c) + y_c;


        if (almostEqual(x_i_ab, x_i_cd, eps) && almostEqual(y_i_ab, y_i_cd, eps)) {
            return new Point(x_i_ab, y_i_ab);
        }


        return null;

    }
}

package td1;

/**
 * Cette classe représente les points du plan cartésien.
 *
 * @author V. Granet vg@unice.fr
 */
public class Point {
    private double x, y;
    private String name;

    /*
     * Rôle : initialise le point (0,0)
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    /*
     * Rôle : initialise le point (x,y)
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @param x:    x coordinate
     * @param y:    y coordiante
     * @param name: name of the point
     */
    public Point(double x, double y, String name) {
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Rôle : renvoie l'abscisse du Point courant
     */
    public double getX() {
        return this.x;
    }

    /**
     * Rôle : renvoie l'ordonnée du Point courant
     */
    public double getY() {
        return this.y;
    }

    /**
     * Rôle :  change l'abscisse du Point courant
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Rôle : change l'ordonnée du Point courant
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Rôle : teste si le Point courant est égal à au Point p
     */
    public boolean equals(Point p) {
        return this.getX() == p.getX() && this.getY() == p.getY();
    }

    /**
     * Rôle : envoie la distance entre le Point courant et le Point p
     * Rappel : distance(a,b) = rac2((b_x-a-x)^2+(b_y-a-y)^2)
     */
    public double distance(Point p) {
        return Math.sqrt(Math.pow(p.getX() - this.getX(), 2) + Math.pow(p.getY() - this.getY(), 2));
    }

    /**
     * Rôle : renvoie la représentation du Point courant sous
     * forme d'une chaîne de caratères
     */
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", name='" + name + '\'' +
                '}';
    }
}

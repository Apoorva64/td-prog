package other.testing.pad.perfomance.simulation.montecarlo.pi.console;
import it.unimi.dsi.util.XoRoShiRo128PlusRandom;

public class Thread extends java.lang.Thread {
    private static final long ITERATION_AMOUNT = 10000000000L;
    private long pointCount;
    private long pointCountInCircle;
    private final XoRoShiRo128PlusRandom randomGenerator = new XoRoShiRo128PlusRandom();
//    public static double pi;

    public long getPointCount() {
        return pointCount;
    }

    public long getPointCountInCircle() {
        return pointCountInCircle;
    }

    private double getRandomNumber() {
        return ((this.randomGenerator.nextDouble() * ((double) 1 - (double) -1)) + (double) -1);
    }

    @Override
    public void run() {
        for (long i = 0; i < ITERATION_AMOUNT; i++) {
            this.pointCount += 1;
            double distance = Math.sqrt(Math.pow(getRandomNumber(), 2) + Math.pow(getRandomNumber(), 2));
            if (distance < 1) {
                this.pointCountInCircle += 1;
            }
        }
    }
}

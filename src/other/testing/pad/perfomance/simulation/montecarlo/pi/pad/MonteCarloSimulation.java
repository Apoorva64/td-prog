package other.testing.pad.perfomance.simulation.montecarlo.pi.pad;

import PaD.*;
import td1.Plan2D;
import td1.Point;

import java.awt.*;


public class MonteCarloSimulation {
    private static final double size = 1000;
    private static final boolean DRAWING = true;

    public static double getRandomNumber(double min, double max) {
        return ((Math.random() * (max - min)) + min);
    }

    public static void main(String[] args) {
        final Point origin = new Point();
        final Plan2D pad = new Plan2D((int) (size / 1.5), (int) (size / 1.5));
        final Point originDrawSpace = pad.convertCoordinates(origin);
        final PlancheADessin canvas = pad.getPad();


        final Cercle circle = new Cercle(originDrawSpace.getX(), originDrawSpace.getY(), size / 2);
        canvas.ajouter(circle);

        final Carré square = new Carré(originDrawSpace.getX() - size / 4, originDrawSpace.getY() - size / 4, size / 2);
        canvas.ajouter(square);


        final double pointSize = 3;
        long pointCount = 1L;
        long pointCountInCircle = 1L;
        final double idkWhyFactor = size / 80;
        Dessinable piText = new Texte(originDrawSpace.getX() + idkWhyFactor/5, idkWhyFactor/5, Double.toString(0), Color.BLACK);
        Dessinable pointNumberText = new Texte(idkWhyFactor/5, idkWhyFactor/5, Double.toString(0), Color.BLACK);
        final int updateCycle;
        if (DRAWING) {
            updateCycle = 100;
        } else {
            updateCycle = 1000000;
        }


        while (pointCount<Long.MAX_VALUE) {
            Point point = new Point(getRandomNumber(-idkWhyFactor, idkWhyFactor), getRandomNumber(-idkWhyFactor, idkWhyFactor));
            pointCount += 1;

            if (point.distance(origin) < size / 80) {
                if (DRAWING) {
                    pad.drawPoint(point, Color.red, pointSize);
                }
                pointCountInCircle += 1;
            } else {
                if (DRAWING) {
                    pad.drawPoint(point, Color.cyan, pointSize);
                }
            }

            if (pointCount % (updateCycle) == 0) {
                double pi = 4 * (double)pointCountInCircle / pointCount;
                canvas.supprimer(piText);
                canvas.supprimer(pointNumberText);
                piText = new Texte(originDrawSpace.getX() + idkWhyFactor, idkWhyFactor, "pi = " + pi, Color.BLACK);
                pointNumberText = new Texte(idkWhyFactor, idkWhyFactor, "points: " + pointCount, Color.BLACK);
                canvas.ajouter(piText);
                canvas.ajouter(pointNumberText);

            }

        }


    }

}

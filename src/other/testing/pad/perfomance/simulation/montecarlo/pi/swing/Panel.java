package other.testing.pad.perfomance.simulation.montecarlo.pi.swing;

import td1.Point;

import javax.swing.*;
import java.awt.*;

import static other.testing.pad.perfomance.simulation.montecarlo.pi.pad.MonteCarloSimulation.getRandomNumber;

public class Panel extends JPanel {
    private final int WIDTH = 800;
    private final int HEIGHT = 800;
    private long pointCount = 1L;
    private long pointCountInCircle = 1L;
    private final Point origin = new Point((double) WIDTH / 2, (double) HEIGHT / 2);
    private final JLabel piLabel;

    public Panel(JLabel piLabel) {
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        piLabel.setSize(WIDTH / 3, 50);
        this.piLabel = piLabel;


    }


    public void paint(Graphics g) {
        Graphics2D g2D = (Graphics2D) g;
        g2D.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2D.drawOval(0, 0, WIDTH, HEIGHT);

        for (int i = 0; i < 10000; i++) {
            Point point = new Point(getRandomNumber(0, WIDTH), getRandomNumber(0, HEIGHT));
            pointCount += 1;

            if (point.distance(origin) < (double) WIDTH / 2) {
                g2D.setColor(Color.red);
                g2D.drawLine((int) point.getX(), (int) point.getY(), (int) point.getX(), (int) point.getY());
                pointCountInCircle += 1;
            } else {
                g2D.setColor(Color.cyan);
                g2D.drawLine((int) point.getX(), (int) point.getY(), (int) point.getX(), (int) point.getY());
            }
        }

        double pi = 4 * (double) pointCountInCircle / pointCount;
        this.piLabel.setText("pi = " + String.format("%,.010f", pi) + ", points: " + pointCount);
        repaint();

    }
}

package other.testing.pad.perfomance.simulation.montecarlo.pi.swing;

import javax.swing.*;

public class JFrame extends javax.swing.JFrame {
    public JFrame(){
        this.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        JLabel piLabel = new JLabel("");
        piLabel.setVisible(true);
        Panel panel = new Panel(piLabel);
        this.add(piLabel);
        this.add(panel);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setResizable(false);
    }
    public static void main(String[] args){
        new JFrame();
    }

}

package other.testing.pad.perfomance.simulation.montecarlo.pi.console;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        long pointCount;
        long pointCountInCircle;
        Thread[] threads = new Thread[10];
        for (int i = 0; i < threads.length; i++) {
            threads[i]=  new Thread();
        }
        for (Thread thread : threads) {
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        pointCount = 0;
        pointCountInCircle = 0;
        for (Thread thread : threads) {
            pointCount += thread.getPointCount();
            pointCountInCircle += thread.getPointCountInCircle();
        }

        System.out.println(4 * (double) pointCountInCircle / pointCount);
        System.out.println(pointCount);

    }
}

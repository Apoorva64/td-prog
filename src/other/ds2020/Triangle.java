package other.ds2020;

import td1.Point;

import java.util.ArrayList;
import java.util.HashMap;

import static other.ds2020.Vertices.*;


public class Triangle {
    private Point p1, p2, p3;
    private final HashMap<Vertices,Point> points;

    public Triangle(Point p1, Point p2, Point p3) {
        if ((!p1.equals(p2) && !p1.equals(p3)) && (!p2.equals(p3))) {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.points= new HashMap<>();
            this.points.put(S1,p1);
            this.points.put(S2,p2);
            this.points.put(S3,p3);
            ArrayList<Point> points2 = new ArrayList<>();
            points2.add(p1);
            points2.add(p2);
            points2.add(p3);
            System.out.println(points2.get(1));

            for (Point point : points2) {
                System.out.println(point);
            }


        } else {
            throw new IllegalArgumentException("Les coordonne sont egaux trinagle pas marcher");
        }
    }

    public Point getSommet(Vertices sommet) {
//        if (S1=="S1")
        return this.points.get(sommet);

//        if (sommet == S1) {
//            return this.p1;
//        }
//        if (sommet == S2) {
//            return this.p2;
//        }
//        if (sommet == S3) {
//            return this.p3;
//        }
//        return null;
    }

    public void setSommet(Vertices sommet, Point point) {
        switch (sommet) {
            case S1:
                this.p1 = point;
                break;
            case S2:
                this.p2 = point;
                break;
            case S3:
                this.p3 = point;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sommet);
        }


    }

    public static void main(String[] args) {
        Triangle triangle = new Triangle(new Point(), new Point(1, 2), new Point(2, 3));
        triangle.setSommet(S2, new Point(3, 4));
        System.out.println(triangle.getSommet(S2));
    }

}

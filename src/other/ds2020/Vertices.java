package other.ds2020;

import td1.Point;

public enum Vertices {
    S1, S2, S3;
    private Point point;
    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}

package td3;

import td3.card.CardColor;
import td3.card.CardValue;

public class Test {
    public static void main(String[] args) {
        // Q2
        CardValue v = CardValue.ACE;
        System.out.println(v);

        // Q3
        for (CardValue cardValue :
                CardValue.values()) {
            System.out.println(cardValue);
        }

        // Q4
        for (CardValue cardValue :
                CardValue.values()) {
            System.out.print(cardValue + ": " + cardValue.ordinal() + " ");
        }
        System.out.println();
        for (CardColor cardColor :
                CardColor.values()) {
            System.out.print(cardColor + ": " + cardColor.ordinal() + " ");
        }
        System.out.println();
        for (CardValue cardValue :
                CardValue.values()) {
            System.out.print(cardValue + ": " + cardValue.getValue() + " ");
        }
        System.out.println();


    }
}

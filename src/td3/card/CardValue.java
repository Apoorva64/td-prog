package td3.card;

/**
 * Represents the value of a card
 */
public enum CardValue {
    TWO(2, "deux"), THREE(3, "trois"), FOUR(4, "quatre"),
    FIVE(5, "cinq"), SIX(6, "six"), SEVEN(7, "sept"), EIGHT(8, "huit"),
    NINE(9, "neuf"), TEN(10, "dix"), JACK(10, "valet"), QUEEN(10, "dame"),
    KING(10, "roi"), ACE(20, "as");
    private final Integer value;
    private final String name;

    CardValue(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getValue() {
        return value;
    }
}

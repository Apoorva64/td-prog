package td3.card;

/**
 * Represents the color of card
 */
public enum CardColor {
    CLUBS("trèfle"), DIAMONDS("carreau"), HEARTS("coeur"), SPADES("pique");
    private final String name;

    CardColor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

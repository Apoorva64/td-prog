package td3;

import PaD.PlancheADessin;
import td3.card.Card;
import td3.card.CardColor;
import td3.card.CardValue;

public class Game {

    public static void main(String[] args) {
        Card card1 = new Card(CardValue.QUEEN, CardColor.SPADES);
        Card card2 = new Card(CardValue.SIX, CardColor.CLUBS);
//        Card card1 = new Card(CardValue.JACK, CardColor.CLUBS);
//        Card card2 = new Card(CardValue.JACK, CardColor.DIAMONDS);

        System.out.println(card1 + "" + card2);

        System.out.println(card1.compareTo(card2));

        PlancheADessin pad = new PlancheADessin();
        card1.draw(pad,100,100);
        card2.draw(pad,200,100);
        card1.flipCard(pad);
    }

}

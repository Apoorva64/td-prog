package td5;

import PaD.PlancheADessin;
import td5.beforeQ20.Figure;
import td5.beforeQ20.Point;
import td5.afterQ20.*;

import java.util.Random;

public class Test {
    public static final Random randomGenerator = new Random();

    public static void main(String[] args) {
        Q16();
        Q17();
        Q18();
        Q22();
        Q23();
        Q25();
    }

    public static void Q15() {
        Rectangle r1 = new Square(8);
//        incompatible types: td5.aT1.Circle cannot be converted to td5.aT1.Rectangle as Rectangle doesn't inherit from Circle
//        Rectangle r2 = new Circle(8);
    }

    public static void Q16() {
//        incompatible types: td5.aT1.Rectangle cannot be converted to td5.aT1.Square as Square doesn't inherit from Rectangle
//        Square r2 = new Rectangle(2,8);


    }

    public static void Q17() {
        Rectangle r1 = new Square(8);
//        td5.aT1.Rectangle cannot be converted to td5.aT1.Square as Square doesn't inherit from Rectangle
//        Square r2 = r1;
        Square r2 = (Square) r1;
    }

    public static void Q18() {
        Figure[] figures = new Figure[]{
                new Figure(new Point(2, 3)),
                new td5.beforeQ20.Rectangle(9, 3),
                new td5.beforeQ20.Square(4),
                new td5.beforeQ20.Circle(4),
        };

        System.out.println(((td5.beforeQ20.Rectangle) figures[1]).calculatePerimeter());
        System.out.println(((td5.beforeQ20.Rectangle) figures[1]).calculateSurface());

        System.out.println(((td5.beforeQ20.Square) figures[2]).calculatePerimeter());
        System.out.println(((td5.beforeQ20.Square) figures[2]).calculateSurface());

        System.out.println(((td5.beforeQ20.Circle) figures[3]).calculatePerimeter());
        System.out.println(((td5.beforeQ20.Circle) figures[3]).calculateSurface());

        // td5.before.Figure cannot be cast to class td5.before.Rectangle
//        System.out.println(((td5.before.Rectangle) figures[0]).calculatePerimeter());
//        System.out.println(((td5.before.Rectangle) figures[0]).calculateSurface());
    }

    public static FigureAbstract getRandomFigure() {
        int figureType = randomGenerator.nextInt(4);
        int maxRandomValue = 1000;
        switch (figureType) {
            case 0: {
                return new Ellipse(new td5.afterQ20.Point(
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue)),
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue));
            }
            case 1: {
                return new Circle(new td5.afterQ20.Point(
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue)),
                        randomGenerator.nextInt(maxRandomValue));
            }
            case 2: {
                return new Rectangle(new td5.afterQ20.Point(
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue)),
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue));
            }
            case 3: {
                return new Square(new td5.afterQ20.Point(
                        randomGenerator.nextInt(maxRandomValue),
                        randomGenerator.nextInt(maxRandomValue)),
                        randomGenerator.nextInt(maxRandomValue));
            }


        }
        return new Circle(0);

    }

    public static void Q22() {
        FigureAbstract[] figures = new FigureAbstract[30];
        for (int i = 0; i < figures.length; i++) {
            figures[i] = getRandomFigure();
        }
    }

    public static void Q23() {
        FigureAbstract[] figures = new FigureAbstract[30];
        for (int i = 0; i < figures.length; i++) {
            figures[i] = getRandomFigure();
            System.out.println(figures[i].calculatePerimeter());
            System.out.println(figures[i].calculateSurface());
        }
    }

    public static void Q25() {
        PlancheADessin canvas = new PlancheADessin();
        FigureAbstract[] figures = new FigureAbstract[100];
        for (int i = 0; i < figures.length; i++) {
            figures[i] = getRandomFigure();
            System.out.println(figures[i].calculatePerimeter());
            System.out.println(figures[i].calculateSurface());
            figures[i].draw(canvas);
        }
    }

}

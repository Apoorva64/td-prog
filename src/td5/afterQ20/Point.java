package td5.afterQ20;

import PaD.CerclePlein;
import PaD.PlancheADessin;

import java.util.Objects;

public class Point {
    protected double x,y;
    public static int POINT_SIZE = 10;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public Point() {
        this(0,0);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void draw(PlancheADessin canvas){
        canvas.ajouter(new CerclePlein(this.getX(), this.getY(), POINT_SIZE));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 && Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}

package td5.afterQ20;

import PaD.PlancheADessin;

public abstract class FigureAbstract {
    protected Point origin;

    protected FigureAbstract(Point origin) {
        this.origin = origin;
    }

    protected FigureAbstract() {
        this(new Point(0, 0));
    }

    public abstract double calculatePerimeter();

    public abstract double calculateSurface();

    protected Point getOrigin() {
        return origin;
    }

    protected void setOrigin(Point origin) {
        this.origin = origin;
    }

    public void draw(PlancheADessin canvas) {
        this.origin.draw(canvas);
    }

}

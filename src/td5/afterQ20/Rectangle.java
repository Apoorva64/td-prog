package td5.afterQ20;

import PaD.PlancheADessin;

public class Rectangle extends FigureAbstract {
    protected double length, height;

    public Rectangle(Point origin, double length, double height) {
        super(origin);
        this.length = length;
        this.height = height;
    }

    public Rectangle(double length, double height) {
        super(new Point());
        this.length = length;
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double calculatePerimeter() {
        return 2 * (length + height);
    }

    public double calculateSurface() {
        return length * height;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        string.append("+").append("--".repeat((int) this.length)).append("+\n");
        for (int height = 0; height < this.height - 2; height++) {
            string.append("|").append("  ".repeat((int) this.length)).append("|\n");
        }
        string.append("+").append("--".repeat((int) this.length)).append("+\n");
        return string.toString();
    }

    @Override
    public void draw(PlancheADessin canvas) {
        canvas.ajouter(new PaD.Rectangle(this.origin.getX(), this.origin.getY(), this.length, this.height));
    }
}

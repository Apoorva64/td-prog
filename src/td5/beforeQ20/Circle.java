package td5.beforeQ20;

public class Circle extends Ellipse {
    public Circle(Point origin, double a) {
        super(origin, a, a);
    }

    public Circle(double diameter) {
        super(diameter,diameter);
    }

    @Override
    public void setA(double a) {
        this.a = a;
        this.b = a;
    }

    @Override
    public void setB(double b) {
        this.a = b;
        this.b = b;
    }
}

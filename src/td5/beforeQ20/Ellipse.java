package td5.beforeQ20;

import PaD.PlancheADessin;

public class Ellipse extends Figure {
    protected double a, b;

    public Ellipse(Point origin, double a, double b) {
        super(origin);
        this.b = b;
        this.a = a;
    }

    public Ellipse(double a, double b) {
        this(new Point(), a, b);

    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double calculatePerimeter() {
        return Math.PI * Math.sqrt(2 * (Math.pow(a, 2) + Math.pow(b, 2)));
    }

    public double calculateSurface() {
        return Math.PI * a * b;
    }

    public void draw(PlancheADessin canvas) {
        canvas.ajouter(new PaD.Ellipse(this.origin.getX(), this.origin.getY(), this.a, this.b));
    }
}

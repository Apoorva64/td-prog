package td4;

import PaD.PlancheADessin;

import java.util.Objects;
import java.util.Scanner;

import static java.lang.Math.random;

public class CardGame52Test {
    public static void main(String[] args) {
        Q11();
//        Q12();
//        Q14();
//        Q18();
//        Q20();
//        Q24();
    }

    public static void Q11(){
        CardGame52 game = new CardGame52();
        CardGame52 game2 = new CardGame52();
        game.mixCards();
        game.sortCards();
        System.out.println(game);
        System.out.println(game2);



    }


    public static void Q12() {
        PlancheADessin canvas = new PlancheADessin(1000, 700);
        CardGame52 game = new CardGame52();
        System.out.println(game.getCard(0));
        System.out.println(game);
        game.draw(canvas);
    }

    public static void Q14() {
        PlancheADessin canvas = new PlancheADessin(1000, 700);
        CardGame52 game = new CardGame52();
        game.mixCards();
        game.draw(canvas);
        Scanner scanner = new Scanner(System.in);
        System.out.print("eecrivez un int pour ordonner les cartes");
        scanner.nextInt();
        game.sortCards();
        game.draw(canvas);


    }

    public static void Q18() {
        PlancheADessin canvas = new PlancheADessin(1000, 700);
        CardGame52 game = new CardGame52();
        Player player1 = new Player("player1");
        player1.takeCards(game, 0, 13);
        player1.draw(canvas, 200);
    }

    public static void Q20() {
        PlancheADessin canvas = new PlancheADessin(1000, 700);
        CardGame52 game = new CardGame52();
        game.mixCards();
        Player[] players = new Player[]{
                new Player("Isabelle"),
                new Player("Pierre"),
                new Player("Zonra"),
                new Player("Mei")};
        int cardCount = 0;
        for (Player player :
                players) {
            player.takeCards(game, cardCount, cardCount + CardGame52.CARD_AMOUNT / players.length);
            cardCount += CardGame52.CARD_AMOUNT / players.length;

        }

        int y = 0;
        for (Player player :
                players) {
            player.draw(canvas, y);
            y += player.getDrawHeight();
        }
    }

    public static void Q24() {
        PlancheADessin canvas = new PlancheADessin(1000, 700);
        CardGame52 game = new CardGame52();
        game.mixCards();
        Player[] players = new Player[]{
                new Player("Isabelle"),
                new Player("Pierre"),
                new Player("Zonra"),
                new Player("Mei")};
        int cardCount = 0;
        for (Player player :
                players) {
            player.takeCards(game, cardCount, cardCount + CardGame52.CARD_AMOUNT / players.length);
            cardCount += CardGame52.CARD_AMOUNT / players.length;

        }

        int y = 0;
        for (Player player :
                players) {
            player.draw(canvas, y);
            y += player.getDrawHeight();
        }
        for (int i = 0; i < 10; i++) {
            game.getCards()[(int) (random() * 52)].flipCard(canvas);
        }
    }
}


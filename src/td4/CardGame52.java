package td4;

import PaD.PlancheADessin;
import td4.card.Card;
import td4.card.CardColor;
import td4.card.CardValue;


import java.util.Arrays;

import static java.lang.Math.random;
import static td4.BrokenPadDimensionsFix.X_PAD;
import static td4.BrokenPadDimensionsFix.Y_PAD;

public class CardGame52 {
    protected final Card[] currentCards;
    public static final int CARD_AMOUNT = 52;

    public CardGame52() {
        this.currentCards = new Card[52];
        int i = 0;
        for (CardColor color : CardColor.values()) {
            for (CardValue value :
                    CardValue.values()) {
                this.currentCards[i] = new Card(value, color);
                i++;
            }
        }

    }

    /**
     * Gets a specific card at a given index
     *
     * @param index : index of the card in currentCards array
     * @return: the card at index
     */
    public Card getCard(int index) {
        if (index >= 0 && index <= this.currentCards.length - 1) {
            return this.currentCards[index];
        }
        return null;
    }

    /**
     * Gets the current cards
     *
     * @return : a shallow copy of currentCards
     */
    public Card[] getCards() {
        return this.currentCards.clone();
    }

    /**
     * Mixes cards in currentCards by swapping 2 cards in the array 52 times
     */
    public void mixCards() {
        for (int i = 0; i <= 52; i++) {
            int elementIndex1 = (int) (random() * (this.currentCards.length - 1));
            int elementIndex2 = (int) (random() * (this.currentCards.length - 1));
            Card element1 = this.currentCards[elementIndex1];
            Card element2 = this.currentCards[elementIndex2];
            this.currentCards[elementIndex1] = element2;
            this.currentCards[elementIndex2] = element1;
        }
    }

    /**
     * Sorts the cards in currentCards with their compareTo() method
     */
    public void sortCards() {


        for (int i = 0; i < this.currentCards.length; i++) {
            // find the minimum of an iteration
            Card minCard = new Card(CardValue.ACE, CardColor.SPADES);
            int minCardIndex = 0;
            for (int j = i; j < this.currentCards.length; j++) {
                Card card = this.currentCards[j];
                if (card.compareTo(minCard) <= 0) {
                    minCard = card;
                    minCardIndex=j;
                }
            }
            swap(this.currentCards,minCardIndex,i);

        }
//        Arrays.sort(this.currentCards);
    }
    public static void swap(Card[] cards,int index1,int index2){
        Card card1= cards[index1];
        Card card2= cards[index2];
        cards[index1]=card2;
        cards[index2]=card1;

    }

    @Override
    public String toString() {
        return "CardGame52{" +
                "currentCards=" + Arrays.toString(currentCards) +
                '}';
    }

    /**
     * Draws the full card deck on screen
     *
     * @param canvas: canvas to draw on
     */
    public void draw(PlancheADessin canvas) {
        int cardIndex = 0;
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 13; x++) {
                currentCards[cardIndex].draw(canvas, X_PAD.getValue() + x * Card.TEXTURE_DIMENSION[0],
                        Y_PAD.getValue() + y * Card.TEXTURE_DIMENSION[1]);
                cardIndex++;
            }
        }
    }

}

package td4;

import PaD.PlancheADessin;
import PaD.Texte;
import td4.card.Card;

import java.awt.*;
import java.util.Arrays;

import static td4.BrokenPadDimensionsFix.*;

/**
 * Card game player class
 */
public class Player {
    private final String name;
    private Card[] cards;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


    public void takeCards(CardGame52 game, int startIndex, int endIndex) {
        this.cards = Arrays.copyOfRange(game.getCards(), startIndex, endIndex);
    }

    /**
     * Calculates the size on th ey axis of the player banner(cards+ text)
     *
     * @return y: returns the height of the player banner
     */
    public int getDrawHeight() {
        int y = 0;
        y += FONT_HEIGHT.getValue();
        y += Card.TEXTURE_DIMENSION[1];
        return y;

    }

    /**
     * Draws the player banner(player name+ player cards) on the canvas
     *
     * @param pad canvas: canvas to draw on
     * @param y:  y position of the start of the drawing
     */
    public void draw(PlancheADessin pad, int y) {
        //fix pad
        y += Y_PAD.getValue();

        // draw text
        Texte text = new Texte(X_PAD.getValue(), y, this.name, Color.BLACK);
        pad.ajouter(text);

        // draw Cards
        Card[] cards1 = this.cards;
        for (int x = 0; x < cards1.length; x++) {
            Card card = cards1[x];
            card.draw(pad, X_PAD.getValue() + x * Card.TEXTURE_DIMENSION[0], y + FONT_HEIGHT.getValue());
        }
    }
}

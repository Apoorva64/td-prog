package td4;

/**
 * Enum containing random values to fix padding issues in PaD
 */
public enum BrokenPadDimensionsFix {
    X_PAD(10), Y_PAD(12), FONT_HEIGHT(14 * 2);
    private final int value;

    BrokenPadDimensionsFix(int x) {
        this.value = x;
    }

    public int getValue() {
        return value;
    }
}

package td4.card;

import PaD.Image;
import PaD.PlancheADessin;

import java.util.Objects;

/**
 * Card class
 */
public class Card implements Comparable<Card> {
    private final CardColor color;
    private final CardValue value;
    private final Image texture;
    private final Image cardBack = new Image("./data/cards/" + "dos.gif");
    private double x, y;
    public static final double[] TEXTURE_DIMENSION = new double[2];


    public Card(CardValue value, CardColor color) {
        this.color = color;
        this.value = value;
        this.texture = new Image("./data/cards/" + this.value.getName() + "-" + this.color.getName() + ".gif");
        TEXTURE_DIMENSION[0] = cardBack.getLargeur();
        TEXTURE_DIMENSION[1] = cardBack.getHauteur();
    }

    public CardColor getColor() {
        return color;
    }

    public CardValue getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "(" + value.getValue() + ")" + "," + color + ']';
    }


    /**
     * method to compare two cards
     *
     * @param other : other card to compare to
     * @return: a positive number if current card is superior, 0 is current card is equal, a negative number if current card is inferior
     */
    public int compareTo(Card other) {
        Integer cardColor = this.getColor().ordinal();
        Integer otherColor = other.getColor().ordinal();
        int cardColorComparison = cardColor.compareTo(otherColor);
        if (cardColorComparison != 0) {
            return cardColorComparison;
        }
        return this.getValue().compareTo(other.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return color == card.color && value == card.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, value, texture);
    }

    /**
     * Draws the card on a certain xy position on a certain canvas
     *
     * @param canvas: canvas to draw on
     * @param x:      x position of the card
     * @param y:      y position of the card
     */
    public void draw(PlancheADessin canvas, double x, double y) {
        this.texture.setOrig(x, y);
        this.x = x;
        this.y = y;
        canvas.ajouter(this.texture);
    }

    /**
     * Flips the card by changing its representation to a filliped card
     *
     * @param canvas : canvas to draw on
     */
    public void flipCard(PlancheADessin canvas) {
        canvas.supprimer(this.texture);
        cardBack.setOrig(this.x, this.y);
        canvas.ajouter(cardBack);
    }

}

package td0;

import java.util.Objects;

public class Rectangle {
    private double x;
    private double y;
    private double width;
    private double height;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.getX(), getX()) == 0 && Double.compare(rectangle.getY(),
                getY()) == 0 && Double.compare(rectangle.getWidth(),
                getWidth()) == 0 && Double.compare(rectangle.getHeight(),
                getHeight()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getWidth(), getHeight());
    }

    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "TD0.Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public void setHeight(double height) {
        this.height = height;
    }
}

package td0;

import java.util.Scanner;

public class Moyenne {
    public static void main(String[] args) {
        // get input
        Scanner scanner = new Scanner(System.in);
        double[] inputs = new double[3];
        for (int i = 0; i < 3; i++) {
            System.out.print("nb " + i + ":");
            inputs[i] = scanner.nextDouble();
        }
        // calculate and print
        System.out.println("la moyenne est " + mean(inputs));
    }

    /**
     * @param array : array of double numbers to calculates the mean of
     * @return: the mean of the numbers
     */
    public static double mean(double[] array) {
        double sum = 0;
        for (double item : array) {
            sum += item;
        }
        return sum / array.length;
    }
}

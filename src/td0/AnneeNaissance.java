package td0;

import java.util.Scanner;
import java.util.Calendar;

public class AnneeNaissance {
    public static void main(String[] args) {
        // get input field
        Scanner scanner = new Scanner(System.in);
        // get input age from user
        System.out.print("name: ");
        String name = scanner.nextLine();
        System.out.print("age: ");
        int age = scanner.nextInt();
        Calendar cal = Calendar.getInstance();
        int current_year = cal.get(Calendar.YEAR);
        // print
        System.out.println(name + " vous etes nee en " + (current_year - age));
    }
}
package td0;

import PaD.*;

public class Dessin {
    public static void main(String[] args) {
        PlancheADessin pad = new PlancheADessin();
        double milieu = pad.getLargeur() / 2;

        // creating shapes
        Dessinable titre = new Texte(milieu - 60, 10, "Mon Bonhomme");
        Dessinable tete = new CerclePlein(milieu, 80, 60, PlancheADessin.ROUGE);
        Dessinable cou = new Ligne(milieu, 110, milieu, 170);
        Dessinable corps = new RectanglePlein(milieu - 40, 170, 80, 100,
                PlancheADessin.VERT);
        // draw arms
        Dessinable left_arm = new Ligne(milieu, milieu - 200, milieu + 100, milieu - 150);
        Dessinable right_arm = new Ligne(milieu, milieu - 200, milieu - 100, milieu - 150);
        // draw legs
        Dessinable left_leg = new Ligne(milieu, milieu - 200, milieu - 100, milieu + 50);
        Dessinable right_leg = new Ligne(milieu, milieu - 200, milieu + 100, milieu + 50);


        // adding to drawing board
        pad.ajouter(left_arm);
        pad.ajouter(right_arm);
        pad.ajouter(left_leg);
        pad.ajouter(right_leg);

        pad.ajouter(titre);
        pad.ajouter(tete);
        pad.ajouter(cou);
        pad.ajouter(corps);
    }
}
